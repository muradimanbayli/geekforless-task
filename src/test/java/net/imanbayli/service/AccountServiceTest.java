package net.imanbayli.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

import org.assertj.core.api.Assertions;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import net.imanbayli.entity.Account;
import net.imanbayli.model.AccountRequest;
import net.imanbayli.model.AccountResponse;
import net.imanbayli.repository.AccountRepository;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {
  @InjectMocks AccountService accountService;
  @Mock AccountRepository accountRepository;

  @Test
  void createGivenAccountRequestExpectNewAccountCreated() {
    //setup
    Account account = Instancio.create(Account.class);
    account.setBalance(100);
    AccountRequest accountRequest = new AccountRequest();
    accountRequest.setAmount(account.getBalance());
    Mockito.when(accountRepository.save(any())).thenReturn(account);
    //when
    AccountResponse accountResponse = accountService.create(accountRequest);
    //expect
    Assertions.assertThat(accountResponse).isNotNull();
    Assertions.assertThat(accountResponse.getAmount()).isEqualTo(accountRequest.getAmount());
    Mockito.verify(accountRepository).save(any());
  }

  @Test
  void getGivenValidIbanExpectAccountReturned() {
    //setup
    Account account = Instancio.create(Account.class);
    Mockito.when(accountRepository.findByIban(account.getIban())).thenReturn(account);
    //when
    AccountResponse accountResponse = accountService.get(account.getIban());
    //expect
    Assertions.assertThat(accountResponse).isNotNull();
    Mockito.verify(accountRepository).findByIban(account.getIban());
  }

}