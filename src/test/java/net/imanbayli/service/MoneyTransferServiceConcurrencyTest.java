package net.imanbayli.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import java.util.Random;

import org.iban4j.Iban;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import net.imanbayli.entity.Account;
import net.imanbayli.repository.AccountRepository;

@SpringBootTest
class MoneyTransferServiceConcurrencyTest {
  Account accountOne;
  Account accountTwo;

  private double transferAmount = 1;
  private double initialAccountBalance = 100_000.00;
  private double sumOfAccountsBalanceBeforeOperations;
  @Autowired private AccountRepository accountRepository;
  @Autowired private AccountService accountService;

  @Autowired private MoneyTransferService moneyTransferService;

  @BeforeEach
  public void setup() {
    accountOne = Account.builder()
        .iban(Iban.random().getAccountNumber())
        .balance(initialAccountBalance)
        .build();

    accountTwo = Account.builder()
        .iban(Iban.random().getAccountNumber())
        .balance(initialAccountBalance)
        .build();
    accountRepository.save(accountOne);
    accountRepository.save(accountTwo);

    sumOfAccountsBalanceBeforeOperations = accountOne.getBalance() + accountTwo.getBalance();
  }

  @Test
  void test_whenMultipleTransfersHappenAtTheSameTimeThenSumOfAccountBalancesAlwaysBeSame_success() throws InterruptedException {
    Thread[] operations = new Thread[2];

    for (int i = 0; i < operations.length; i++) {
      operations[i] = new Thread(this::doTenThousandTransfers);
      operations[i].start();
    }

    for (Thread operation : operations) {
      operation.join();
    }
    moneyTransferService.transferMoney(accountOne.getIban(), accountTwo.getIban(), 10);
    double sumOfBalancesAfterOperations = accountRepository.findByIban(accountOne.getIban()).getBalance()
        + accountRepository.findByIban(accountTwo.getIban()).getBalance();
    assertEquals(sumOfBalancesAfterOperations, sumOfAccountsBalanceBeforeOperations);
  }

  private void doTenThousandTransfers() {
    Random random = new Random();
    for (int j = 0; j < 10_000; j++) {
      int randomIndex = random.nextInt(2);

      String from = randomIndex == 0 ? accountOne.getIban() : accountTwo.getIban();
      String to = randomIndex == 0 ? accountTwo.getIban() : accountOne.getIban();

      moneyTransferService.transferMoney(from, to, transferAmount);
    }
  }

}