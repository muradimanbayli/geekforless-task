package net.imanbayli.model;

import lombok.Data;

@Data
public class AccountRequest {
  private double amount;
}
