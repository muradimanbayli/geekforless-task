package net.imanbayli.model;

import lombok.Data;

@Data
public class TransferRequest {
  private String sourceIban;
  private String targetIban;
  private double amount;
}
