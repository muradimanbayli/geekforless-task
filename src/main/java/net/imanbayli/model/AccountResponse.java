package net.imanbayli.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountResponse {
  private String iban;
  private double amount;
}
