package net.imanbayli.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import net.imanbayli.entity.Account;
import net.imanbayli.repository.AccountRepository;

@Service
@RequiredArgsConstructor
public class MoneyTransferService {
  private final AccountRepository accountRepository;

  @Transactional
  public void transferMoney(String sourceIban, String targetIban, double amount) {
    Account sourceAccount = accountRepository.findByIban(sourceIban);
    Account targetAccount = accountRepository.findByIban(targetIban);

    if (sourceAccount == null || targetAccount == null) {
      throw new IllegalArgumentException("Invalid source or target account.");
    }

    if (sourceAccount.getBalance() < amount) {
      throw new IllegalArgumentException("Insufficient balance.");
    }

    sourceAccount.setBalance(sourceAccount.getBalance() - amount);
    targetAccount.setBalance(targetAccount.getBalance() + amount);
    accountRepository.save(sourceAccount);
    accountRepository.save(targetAccount);
  }

}
