package net.imanbayli.service;

import org.iban4j.Iban;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import net.imanbayli.entity.Account;
import net.imanbayli.model.AccountRequest;
import net.imanbayli.model.AccountResponse;
import net.imanbayli.repository.AccountRepository;

@Service
@RequiredArgsConstructor
public class AccountService {

  private final AccountRepository accountRepository;

  public AccountResponse create(AccountRequest accountRequest) {
    String iban = Iban.random().getAccountNumber();
    Account account = Account.builder()
        .iban(iban)
        .balance(accountRequest.getAmount())
        .build();
    Account saved = accountRepository.save(account);
    return AccountResponse.builder()
        .iban(saved.getIban())
        .amount(saved.getBalance())
        .build();
  }

  public AccountResponse get(String iban) {
    Account account = accountRepository.findByIban(iban);
    return AccountResponse.builder()
        .iban(account.getIban())
        .amount(account.getBalance())
        .build();
  }
}
