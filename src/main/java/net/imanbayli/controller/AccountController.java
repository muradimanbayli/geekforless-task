package net.imanbayli.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import net.imanbayli.model.AccountRequest;
import net.imanbayli.model.AccountResponse;
import net.imanbayli.service.AccountService;

@RestController
@RequestMapping(path = "accounts")
@RequiredArgsConstructor
public class AccountController {

  private final AccountService accountService;

  @GetMapping(path = "{iban}")
  public AccountResponse getAccountDetails(@PathVariable String iban) {
    return accountService.get(iban);
  }

  @PostMapping
  public AccountResponse createAccount(@RequestBody AccountRequest accountRequest) {
    return accountService.create(accountRequest);
  }

}
