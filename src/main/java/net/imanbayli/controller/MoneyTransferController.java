package net.imanbayli.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import net.imanbayli.model.TransferRequest;
import net.imanbayli.service.MoneyTransferService;

@RestController
@RequiredArgsConstructor
public class MoneyTransferController {
  private MoneyTransferService moneyTransferService;

  @PostMapping("/transfer")
  public void transferMoney(@RequestBody TransferRequest request) {
    moneyTransferService.transferMoney(request.getSourceIban(), request.getTargetIban(), request.getAmount());
  }

}
