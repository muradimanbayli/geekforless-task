FROM openjdk:11.0.10-jdk-slim
EXPOSE 8080
USER 1001
COPY build/libs/*.jar /app.jar
CMD java -jar /app.jar
